# Blog

* Napraviti folder za novi projekat: ‘ime-prezime’. 
* Folder treba da sadrži foldere ‘images’ i ‘css’.
* U images staviti nekoliko slika, css će sadržavati vaše stilove.
* Stranice: index.html, about.html, contact.html, add.html, post-1.html, post-2.html i post-3.html.
* Na svim stranicama se nalazi naslov stranice i sidebar koji ima linkove 'Home', 'Add New Post', 'About' i 'Contact'.
* Na index.html stranici se prikazuju tri blog posta i svaki naslov je link ka stranici sa postom, npr. post-1.html.
* Na post-1.html stranicama se nalazi naslov, tekst posta i ime autora.
* Na About stranici se nalazi slika i kratka biografija autora.
* Na Contact stranici prikazati formu za slanje poruke autoru.
* Na Add New Post stranici prikazati formu sa title i body poljima za post.
* Međusobno povezati sve stranice.
